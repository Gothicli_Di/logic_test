import M from "materialize-css";
export default {
  show(msg) {
    M.toast({ html: msg });
  },
};
