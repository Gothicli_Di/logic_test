import axios from "axios";
import toast from "@/toast";
const $api = axios.create({
  baseURL: "/api/",
});

const token = localStorage.getItem("AdminToken");
if (token) {
  $api.defaults.headers["x-access-token"] = token;
}

$api.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    toast.show(error.response?.data?.message);
    return Promise.reject(error);
  }
);

export default $api;
