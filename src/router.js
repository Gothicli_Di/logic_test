import Vue from "vue";
import VueRouter from "vue-router";
import AdminLog from "@/components/AdminLog";
import Greeting from "@/components/content/Greeting";
import Questions from "@/components/content/Questions";
import AdminPanel from "@/components/AdminPanel";
import Final from "@/components/content/Final";
import auth from "@/auth";
Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Greeting,
    },
    {
      path: "/admin",
      component: AdminLog,
      children: [
        {
          path: "/",
          component: Greeting,
          props: { isAdmin: true },
          beforeEnter: (to, from, next) => {
            if (auth.isAuth) {
              next({ name: "adminPanel" });
            } else {
              next();
            }
          },
        },
        {
          path: "panel",
          name: "adminPanel",
          component: AdminPanel,
          meta: { requiresAuth: true },
        },
      ],
    },
    {
      path: "/test",
      component: Questions,
    },
    {
      path: "/final",
      component: Final,
    },
  ],
});

router.beforeEach(async (to, from, next) => {
  const isRequiresAuth = to.matched.some((record) => record.meta?.requiresAuth);
  const isAuthed = auth.isAuth === null ? await auth.checkAuth() : auth.isAuth;
  if (isRequiresAuth) {
    if (!isAuthed) {
      next({ path: "/admin" });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
