import $api from "@/api";
const token = localStorage.getItem("AdminToken");

const auth = {
  isAuth: null,
  async checkAuth() {
    if (!token) {
      this.isAuth = false;
      return false;
    }
    const res = await $api.post("/auth");
    this.isAuth = res.data?.auth;
    return res.data?.auth;
  },
  async doAuth(data) {
    const res = await $api({
      url: "/admin",
      method: "POST",
      data: data,
    });
    $api.defaults.headers["x-access-token"] = res.data?.token;
    localStorage.setItem("AdminToken", res.data?.token);
    this.isAuth = true;
    return res.data?.token;
  },
};

export default auth;
