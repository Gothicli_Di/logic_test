const mongoose = require("mongoose");
const config = require("config");
var bcrypt = require("bcryptjs");
const Admin = require("./backend/models/Admin");
const args = process.argv.slice(2);

connectDB().catch((err) => console.log(err));
async function connectDB() {
  return await mongoose.connect(
    `${config.get("mongoUri")}/${config.get("dbName")}`
  );
}

const actionsList = {
  async "show-admins"() {
    const adminsList = await Admin.find();
    const listFormatted = adminsList.map((a) => a.login).join("\n");
    console.log(listFormatted);
  },
  async "create-admin"(data) {
    const [login, password] = data.split(":");
    const hashedPassword = await bcrypt.hashSync(password, 10);
    const admin = new Admin({ login, password: hashedPassword });
    await admin.save();
  },
};

connectDB().then(async (conn) => {
  const [action, data] = args;
  if (!action) return showHelp();
  actionsList[action](data);
});

function showHelp() {
  console.log("npm run cmd [action] [data] to do smth");
  console.log("npm run cmd create login:password to create admin");
  console.log("npm run cmd show-admins to show admins");
}
