const { Router } = require("express");
const bcrypt = require("bcryptjs");
const User = require("../models/User"); //подключаем модель юзера
const Admin = require("../models/Admin");
const jwt = require("jsonwebtoken");
const config = require("config");
const router = Router();

router.post("/final", async (req, res) => {
  try {
    const { lastname, firstname } = req.body.user;
    const { answers } = req.body;

    const user = new User({ lastname, firstname, answers });
    await user.save();
    res.status(201).json({ message: "Ваши ответы отправлены" });
  } catch (e) {
    res.status(500).json({ message: "Что то пошло не так, попробуйте снова" });
  }
});

router.post("/admin", async (req, res) => {
  try {
    const { login, password } = req.body.admin; //получить поля
    const userAdmin = await Admin.findOne({ login });
    if (!userAdmin) {
      return res.status(400).json({ message: "Такого админа не существует" });
    }
    const isMatch = await bcrypt.compare(password, userAdmin.password);
    if (!isMatch) {
      return res.status(400).json({ message: "Неверный пароль" });
    }
    const generateAccessToken = (idAdmin) => {
      return jwt.sign({ id: idAdmin }, config.get("jwtSecret"), {
        expiresIn: "24h", // 24 часа
      });
    };
    const token = generateAccessToken(userAdmin._id);
    return res.status(200).send({ auth: true, token: token });
  } catch (e) {
    res.status(500).json({
      message: "Что то пошло не так, попробуйте снова",
    });
  }
});

router.post("/auth", function (req, res) {
  const token = req.headers["x-access-token"];
  if (!token) {
    res.status(201).json({ auth: false });
  }
  jwt.verify(token, config.get("jwtSecret"), function (err) {
    if (err) {
      res.status(201).send({ auth: false });
    }
    res.status(201).json({ auth: true });
  });
});

module.exports = router;
