const { Router } = require("express");
const router = Router();
const User = require("../models/User");
const VerifyToken = require("../VerifyToken");

router.get("/interviewersList", async (req, res) => {
  try {
    if (!VerifyToken(req, res)) return;
    const interviewer = await User.find({});
    res.status(200).json(interviewer);
  } catch (e) {
    res.status(500).json({ message: "Данные для вывода таблицы не найдены" });
  }
});

module.exports = router;
