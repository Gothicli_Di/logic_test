const { Router } = require("express");
const bcrypt = require("bcryptjs"); //модуль который хэширует пароли. Нужен ли он вообще?
const config = require("config");
const jwt = require("jsonwebtoken");
const User = require("../models/User"); //подключаем модель юзера
const Admin = require("../models/Admin"); //подключаем модель admin
const router = Router();

/*const hashedPassword = await bcrypt.hash("admin", 12);
console.log(hashedPassword);*/


// /api/auth/register вход в тест для юзера
router.post("/", async (req, res) => {
  try {
    const { lastname, firstname } = req.body.user;
    const user = new User({ lastname, firstname });
    await user.create(); //ждем когда создатся
    res.status(201).json({ message: "Собеседуемый зарегистировался" });
  } catch (e) {
    res.status(500).json({ message: "Что то пошло не так, попробуйте снова" });
  }
});

// /api/auth/login
router.post("/admin", async (req, res) => {
  try {
    const { login, password } = req.body.admin; //получить поля
    const admin = await Admin.findOne({ login });
    if (!admin) {
      return res.status(400).json({ message: "Такого админа не существует" });
    }
    const isMatch = await bcrypt.compare(password, admin.password);
    if (!isMatch) {
      return res.status(400).json({ message: "Неверный пароль" });
    }

    const token = jwt.sign(
      { adminId: admin._id },
      config.get("jwtSecret"),
      { expiresIn: "1h" } //1 час
    );
    res.json({ token, adminId: admin._id });

    /*const admin = new Admin({ login, password });
    await admin.save(); //ждем когда создатся
    res.status(201).json({ message: "Вход в систему выполнен" });*/
  } catch (e) {
    res.status(500).json({ message: "Что то пошло не так, попробуйте снова" });
  }
});

module.exports = router;
