const { Router } = require("express");
const router = Router();
const Question = require("../models/Question");

router.get("/test", async (req, res) => {
  try {
    const questions = await Question.find({});
    res.status(200).json(questions);
  } catch (e) {
    res
      .status(500)
      .json({ message: "Вопросы до вас не дошли, попробуйте снова" });
  }
});

module.exports = router;
