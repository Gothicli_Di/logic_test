const { Router } = require("express");
const router = Router();
const RightAnswers = require("../models/RightAnswers");
const VerifyToken = require("../VerifyToken");

router.get("/rightAnswers", async (req, res) => {
  try {
    if (!VerifyToken(req, res)) return;
    const rightAnswers = await RightAnswers.find({});
    res.status(200).json(rightAnswers);
  } catch (e) {
    res.status(500).json({ message: "Данные о правильных ответах не найдены" });
  }
});

module.exports = router;
