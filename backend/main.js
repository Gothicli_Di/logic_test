const express = require("express");
const app = express();
const config = require("config"); // конфиг с переменными сервера
const bp = require("body-parser");
const mongoose = require("mongoose");
const morgan = require("morgan");
const path = require("path");
const cors = require("cors");
const AuthController = require("./AuthController");

app.use(
  cors({
    origin: "*",
  })
);
app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));
app.use("/api/", require("./routes/Authorization"));
app.use("/api/", require("./routes/Question"));
app.use("/api/", require("./routes/InterviewersList"));
app.use("/api/", require("./routes/RightAnswers"));
app.use(morgan("dev"));

async function start() {
  try {
    await mongoose.connect(config.get("mongoUri"), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      dbName: config.get("dbName"),
    });
    const PORT = config.get("port") || 5000;
    app.listen(PORT, () =>
      console.log(`Приложение запущено на порте ${PORT}...`)
    );
    app.use("/", express.static(path.resolve(__dirname, "../dist")));
    app.get("*", function (req, res) {
      res.sendFile(path.resolve(__dirname, "../dist", "index.html"));
    });
  } catch (e) {
    console.log("Серверная ошибка", e.message);
    process.exit(1);
  }
}
start();
