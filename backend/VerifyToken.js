const jwt = require("jsonwebtoken");
const config = require("config");

function verifyToken(req, res) {
  const token = req.headers["x-access-token"];
  if (!token)
    return res.status(403).send({
      auth: false,
      message: "Вы не авторизированны, чтобы просматривать эти данные",
    });

  return jwt.verify(token, config.get("jwtSecret"), function (err, decoded) {
    if (err) {
      return res.status(500).send({
        auth: false,
        message: "Не удалось проверить вашу авторизацию",
      });
    }

    req.idAdmin = decoded.id;
    return decoded;
  });
}

module.exports = verifyToken;
