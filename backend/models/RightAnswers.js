const { Schema, model } = require("mongoose");

const schema = new Schema({
  questionId: Schema.Types.ObjectId,
  rightAnswer: String,
  NoQuestion: Number,
});
module.exports = model("right_answers", schema);
