const { Schema, model } = require("mongoose");

const schema = new Schema({
  lastname: String,
  firstname: String,
  createdAt: {
    type: Date,
    default: Date.now,
  },
  answers: [
    {
      questionId: Schema.Types.ObjectId,
      selectedAnswer: String,
    },
  ],
});
module.exports = model("users", schema);
