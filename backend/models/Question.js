const { Schema, model } = require("mongoose");

const schema = new Schema({
  No: Number,
  text: String,
  answers: [
    {
      id: String,
      text: String,
    },
  ],
});
module.exports = model("questions", schema);
