const jwt = require("jsonwebtoken");
const config = require("config");
const { Router } = require("express");
const router = Router();

router.get("/admin", function (req, res) {
  const token = req.headers["x-access-token"];
  if (!token)
    return res.status(401).send({
      auth: false,
      message: "Вы не авторизированны, чтобы просматривать эти данные",
    });

  jwt.verify(token, config.get("jwtSecret"), function (err, decoded) {
    if (err)
      return res.status(500).send({
        auth: false,
        message: "Не удалось проверить вашу авторизацию",
      });

    res.status(200).send(decoded);
  });
});

module.exports = router;
