# Production
> Required\
> 🍃 MongoDB\
> 🟢 NodeJS 10+ (**BUT** <17 - there is a bug)

1. [Install NodeJS](https://github.com/nodesource/distributions/blob/master/README.md#debinstall)
2. [Install MongoDB](https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-20-04-ru)
3. [Set Up remote access](https://www.digitalocean.com/community/tutorials/how-to-configure-remote-access-for-mongodb-on-ubuntu-20-04)
4. Run `npm run start` for start the project
5. Run `npm run cmd create-admin login:password` for create admin

## Запустить проект локально

1. В терминале запустить **mongod** "_C:\Users\Proger\Desktop\mongodb\bin\mongod --dbpath "C:\Users\Proger\Documents\logic_test\backend\data\db"_"
2. В терминале запустить **mongo** "_C:\Users\Proger\Desktop\mongodb\bin\mongo_"
3. Запустить **MongoCompass**, ради удобства и, возможно, функциональности проекта))
4. В терминале запустить "**npm run front+back**"
5. Поменять **localhost** если нужно в "_vue.config.js_"
6. Импортировать базу данных из "_backup db test_logic_"
7. Смотреть на порте фронта, а не бека

---
