module.exports = {
  productionSourceMap: false,
  devServer: {
    proxy: {
      "^/api": {
        target: "http://10.31.55.127:5000/",
        ws: true,
        changeOrigin: true,
      },
    },
  },
};
